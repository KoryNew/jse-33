package ru.tsk.vkorenygin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.comparator.ComparatorFileByLastModified;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class FileScanner implements Runnable {

    private final int interval = 3;

    @NotNull
    private final String path = "./";

    @NotNull
    public final Bootstrap bootstrap;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    private Predicate<File> predicateIsFile() {
        return File::isFile;
    }

    @NotNull
    private Predicate<String> predicateIsFileCommand(@NotNull final String fileName) {
        return command -> command.equals(fileName);
    }

    public void init() {
        for (@NotNull final AbstractCommand command : bootstrap.getCommandService().getArguments()) {
            commands.add(command.name());
        }
        es.scheduleWithFixedDelay(this, interval, interval, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        @NotNull final File file = new File(bootstrap.getPropertyService().getFileScannerPath());
        @Nullable final List<File> files = Arrays.asList(file.listFiles());
        files.stream()
                .filter(predicateIsFile())
                .sorted(ComparatorFileByLastModified.getInstance())
                .forEach(item -> commands.stream()
                        .filter(predicateIsFileCommand(item.getName()))
                        .forEach(cmd -> {
                            bootstrap.parseCommand(cmd);
                            item.delete();
                        }));
    }

}

