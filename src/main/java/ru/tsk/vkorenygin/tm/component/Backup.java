package ru.tsk.vkorenygin.tm.component;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    private final static int INTERVAL = 30;

    @NotNull
    private final static String SAVE_COMMAND = "backup-save";

    @NotNull
    private final static String LOAD_COMMAND = "backup-load";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, INTERVAL, INTERVAL, TimeUnit.SECONDS);
    }

    public void save() {
        bootstrap.parseCommand(SAVE_COMMAND);
    }

    public void load() {
        bootstrap.parseCommand(LOAD_COMMAND);
    }

}
