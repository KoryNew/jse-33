package ru.tsk.vkorenygin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Optional;

public interface IProjectRepository extends IOwnerRepository<Project> {

    int getSize();

    @NotNull
    Optional<Project> findByName(@NotNull final String name, @NotNull final String userId);

    @NotNull
    Project changeStatusById(@NotNull final String id, @NotNull final Status status, @NotNull final String userId);

    @NotNull
    Project changeStatusByName(@NotNull final String name, @NotNull final Status status, @NotNull final String userId);

    @NotNull
    Project changeStatusByIndex(@NotNull final Integer index, @NotNull final Status status, @NotNull final String userId);

    @NotNull
    Project startById(@NotNull final String id, @NotNull final String userId);

    @NotNull
    Project startByIndex(@NotNull final Integer index, @NotNull final String userId);

    @NotNull
    Project startByName(@NotNull final String name, @NotNull final String userId);

    @NotNull
    Project finishById(@NotNull final String id, @NotNull final String userId);

    @NotNull
    Project finishByIndex(@NotNull final Integer index, @NotNull final String userId);

    @NotNull
    Project finishByName(@NotNull final String name, @NotNull final String userId);

    @NotNull
    Optional<Project> removeByName(@NotNull final String name, @NotNull final String userId);

}
