package ru.tsk.vkorenygin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IOwnerRepository<Task> {

    int getSize();

    @NotNull
    Optional<Task> findByName(@NotNull final String name, @NotNull final String userId);

    @NotNull
    Task changeStatusById(@NotNull final String id, @NotNull final Status status, @NotNull final String userId);

    @NotNull
    Task changeStatusByName(@NotNull final String name, @NotNull final Status status, @NotNull final String userId);

    @NotNull
    Task changeStatusByIndex(@NotNull final Integer index, @NotNull final Status status, @NotNull final String userId);

    @NotNull
    Task startById(@NotNull final String id, @NotNull final String userId);

    @NotNull
    Task startByIndex(@NotNull final Integer index, @NotNull final String userId);

    @NotNull
    Task startByName(@NotNull final String name, @NotNull final String userId);

    @NotNull
    Task finishById(@NotNull final String id, @NotNull final String userId);

    @NotNull
    Task finishByIndex(@NotNull final Integer index, @NotNull final String userId);

    @NotNull
    Task finishByName(@NotNull final String name, @NotNull final String userId);

    @NotNull
    Task bindTaskToProjectById(@NotNull final String projectId, @NotNull final String taskId, @NotNull final String userId);

    @NotNull
    Task unbindTaskById(@NotNull final String id, @NotNull final String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String id, @NotNull final String userId);

    void removeAllTaskByProjectId(@NotNull final String id, @NotNull final String userId);

    @NotNull
    Optional<Task> removeByName(@NotNull final String name, @NotNull final String userId);

}
