package ru.tsk.vkorenygin.tm.api.controller;

import org.jetbrains.annotations.NotNull;
import ru.tsk.vkorenygin.tm.entity.Project;

public interface IProjectController {

    void create();

    void show(@NotNull final Project project);

    void showById();

    void showByIndex();

    void showByName();

    void showAll();

    void changeStatusById();

    void changeStatusByName();

    void changeStatusByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void updateById();

    void updateByIndex();

    void clear();

}
