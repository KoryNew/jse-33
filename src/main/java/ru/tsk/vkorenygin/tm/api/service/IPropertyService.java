package ru.tsk.vkorenygin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.setting.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getVersion();

    @NotNull
    String getDeveloper();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getValue(@Nullable String key, @Nullable String defaultValue);

    @NotNull
    String getFileScannerPath();

}
