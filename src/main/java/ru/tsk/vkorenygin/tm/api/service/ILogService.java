package ru.tsk.vkorenygin.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILogService {

    void info(@Nullable final String message);

    void debug(@Nullable final String message);

    void command(@Nullable final String message);

    void error(@Nullable final Exception e);

}
