package ru.tsk.vkorenygin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;

import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    protected Predicate<Project> hasName(final String name) {
        return e -> name.equals(e.getName());
    }

    @Override
    public @NotNull Optional<Project> findByName(final @NotNull String name, final @NotNull String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .filter(hasName(name))
                .findFirst();
    }

    @Override
    public @NotNull Project changeStatusById(final @NotNull String id, final @NotNull Status status, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findById(id, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) project.ifPresent(e -> e.setStartDate(new Date()));
        return project.get();
    }

    @Override
    public @NotNull Project changeStatusByName(final @NotNull String name, final @NotNull Status status, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findByName(name, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) project.ifPresent(e -> e.setStartDate(new Date()));
        return project.get();
    }

    @Override
    public @NotNull Project changeStatusByIndex(final @NotNull Integer index, final @NotNull Status status, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) project.ifPresent(e -> e.setStartDate(new Date()));
        return project.get();

    }

    @Override
    public @NotNull Project startById(final @NotNull String id, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findById(id, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return project.get();
    }

    @Override
    public @NotNull Project startByIndex(final @NotNull Integer index, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return project.get();
    }

    @Override
    public @NotNull Project startByName(final @NotNull String name, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findByName(name, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return project.get();
    }

    @Override
    public @NotNull Project finishById(final @NotNull String id, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findById(id, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public @NotNull Project finishByIndex(final @NotNull Integer index, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public @NotNull Project finishByName(final @NotNull String name, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findByName(name, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public @NotNull Optional<Project> removeByName(final @NotNull String name, final @NotNull String userId) {
        @Nullable final Optional<Project> project = findByName(name, userId);
        project.ifPresent(e -> entities.remove(e));
        return project;
    }

}
