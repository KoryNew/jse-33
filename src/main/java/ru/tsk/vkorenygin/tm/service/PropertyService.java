package ru.tsk.vkorenygin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.service.IPropertyService;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "config.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "APPLICATION_VERSION";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_DEVELOPER_KEY = "APPLICATION_DEVELOPER";

    @NotNull
    private static final String APPLICATION_DEVELOPER_DEFAULT = "Unknown";

    @NotNull
    private static final String APPLICATION_DEVELOPER_EMAIL_KEY = "APPLICATION_DEVELOPER_EMAIL";

    @NotNull
    private static final String APPLICATION_DEVELOPER_EMAIL_DEFAULT = "Unknown";

    @NotNull
    private static final String FILESCANNER_PATH_KEY = "FILESCANNER_PATH";

    @NotNull
    private static final String FILESCANNER_PATH_DEFAULT = "";

    @NotNull
    private static final String EMPTY_DEFAULT = "";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getValue(@Nullable final String key, @Nullable final String defaultValue) {
        if (DataUtil.isEmpty(key)) return EMPTY_DEFAULT;
        if (defaultValue == null) return EMPTY_DEFAULT;
        if (System.getProperties().containsKey(key))
            return System.getProperty(key);
        if (System.getenv().containsKey(key))
            return System.getenv(key);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getDeveloper() {
        return properties.getProperty(APPLICATION_DEVELOPER_KEY, APPLICATION_DEVELOPER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return properties.getProperty(APPLICATION_DEVELOPER_EMAIL_KEY, APPLICATION_DEVELOPER_EMAIL_DEFAULT);
    }


    @NotNull
    @Override
    public String getVersion() {
        return getValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull String value = getValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getFileScannerPath() {
        return getValue(FILESCANNER_PATH_KEY, FILESCANNER_PATH_DEFAULT);
    }

}
