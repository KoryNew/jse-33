package ru.tsk.vkorenygin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.service.IAuthService;
import ru.tsk.vkorenygin.tm.api.service.IPropertyService;
import ru.tsk.vkorenygin.tm.api.service.IUserService;
import ru.tsk.vkorenygin.tm.entity.User;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyLoginException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyPasswordException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;
import ru.tsk.vkorenygin.tm.util.DataUtil;
import ru.tsk.vkorenygin.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull private final IUserService userService;

    @NotNull private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public @NotNull User getUser() throws AbstractException {
        final @NotNull String userId = getUserId();
        return userService.findById(userId).get();
    }

    @Override
    public @NotNull String getUserId() throws AccessDeniedException {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final @Nullable String login, final @Nullable String password) throws AbstractException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        final Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().isLocked()) throw new AccessDeniedException();
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (hashPassword == null) throw new AccessDeniedException();
        if (!hashPassword.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        userId = user.get().getId();
    }

    @Override
    public void registry(final @Nullable String login, final @Nullable String password, final @Nullable String email) throws AbstractException {
        userService.create(login, password, email);
    }

    @Override
    public void checkRoles(final Role @Nullable ... roles) throws AbstractException {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item: roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }


}
