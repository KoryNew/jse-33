package ru.tsk.vkorenygin.tm.exception.system;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public final class HashIncorrectException extends AbstractException {

    public HashIncorrectException() {
        super("Error! Hash util doesn't work properly...");
    }

}
