package ru.tsk.vkorenygin.tm.exception.entity;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error! Object not found.");
    }

}
