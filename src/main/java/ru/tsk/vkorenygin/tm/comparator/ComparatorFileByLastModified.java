package ru.tsk.vkorenygin.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorFileByLastModified implements Comparator<File> {

    @NotNull
    private static final ComparatorFileByLastModified INSTANCE = new ComparatorFileByLastModified();

    @NotNull
    public static ComparatorFileByLastModified getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final File o1, @Nullable final File o2) {
        if (o1 == null || o2 == null) return 0;
        @NotNull final Long lastModifiedO1 = o1.lastModified();
        @NotNull final Long lastModifiedO2 = o2.lastModified();
        return lastModifiedO1.compareTo(lastModifiedO2);
    }

}
