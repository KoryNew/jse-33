package ru.tsk.vkorenygin.tm.util;

import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface DataUtil {

    static boolean isEmpty(final @Nullable String value) {
        return (value == null || value.isEmpty());
    }

    static boolean isEmpty(final @Nullable String[] values) {
        return (values == null || values.length == 0);
    }

    static boolean isEmpty(final @Nullable Integer value) {
        return (value == null || value < 0);
    }

    static boolean isEmpty(final @Nullable Collection value) {
        return (value == null || value.isEmpty());
    }

}
