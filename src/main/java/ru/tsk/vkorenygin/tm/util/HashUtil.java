package ru.tsk.vkorenygin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.setting.ISaltSetting;

public interface HashUtil {

    String SECRET = "123456789098765";

    Integer ITERATION = 15975;

    @Nullable
    static String salt(@Nullable final ISaltSetting setting,
                       @Nullable final String value) {
        if (setting == null) return null;
        @Nullable final String secret = setting.getPasswordSecret();
        @Nullable final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @Nullable
    static String salt(@Nullable final String secret,
                       @Nullable final Integer iteration,
                       @Nullable final String value) {
        if (value == null) return null;
        if (secret == null) return null;
        if (iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    static @Nullable String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
