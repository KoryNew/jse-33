package ru.tsk.vkorenygin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.dto.Domain;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";

    @NotNull
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";

    @NotNull
    protected static final String JAXB_SYSTEM_PROP_KEY = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String JAXB_SYSTEM_PROP_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected static final String JAXB_MEDIA_TYPE = "application/json";

    @NotNull
    protected static final String FILE_BACKUP = "./data-backup.xml";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
