package ru.tsk.vkorenygin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-unbind-by-project-id";
    }

    @Override
    public @Nullable String description() {
        return "unbind task from project by id";
    }

    @Override
    public @NotNull Role @NotNull [] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK FROM PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(projectId, currentUserId);
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(taskId, currentUserId);
        serviceLocator.getProjectTaskService().unbindTaskFromProject(projectId, taskId, currentUserId);
    }

}
