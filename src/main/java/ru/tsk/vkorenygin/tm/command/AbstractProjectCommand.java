package ru.tsk.vkorenygin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.tsk.vkorenygin.tm.entity.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public void show(@NotNull Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
        System.out.println("Create date: " + project.getCreateDate());
    }

}
