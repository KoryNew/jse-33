package ru.tsk.vkorenygin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;

public class CommandsCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-сmd";
    }

    @Override
    public @NotNull String name() {
        return "commands";
    }

    @Override
    public @Nullable String description() {
        return "display list of commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<String> commands = serviceLocator.getCommandService().getCommandNames();
        for (final String command : commands) System.out.println(command);
    }

    private void showCommandValue(final String value) {
        if (DataUtil.isEmpty(value)) return;
        System.out.println(value);
    }

}
